from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_descr = fh.read()

setup(
        name='nba_py',
        version='0.1', 
        description="CLI NBA stats, scores and schedules",
        long_description=long_descr,
        url="https://gitlab.com/ssants/nba_py",
        packages=find_packages(),
        install_requires=[
            'nba_api','click','pandas','tableprint', 'blessings'
        ],
        entry_points='''
            [console_scripts]
            nba_py=nba_py.nba_cli:cli
        ''',
)

