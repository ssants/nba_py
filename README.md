# nba_py

This is our final project for CPS 500.

This program will pull data from [stats.nba.com](https://stats.nba.com) using [nba_api](https://github.com/swar/nba_api). Usage can be found in our [guide](/docs/nba_api_usage.md).
Our program is inspired by [nba_go](https://github.com/xxhomey19/nba-go), but we hope to expand our program to other sports as well. 

## Installation

```
git clone https://gitlab.com/ssants/nba_py.git
`
cd nba_py

pip3 install --user .
```

## Dependencies 
Should be installed by pip 
* [nba_api](https://github.com/swar/nba_api)  
	A useful nba api with documented endpoints and static data for faster searching.


* [click](https://click.palletsprojects.com/en/7.x/)  
	A library to make creating command line applications smoother.


* [tableprint](https://github.com/nirum/tableprint)  
	A library for printing data in pretty tables

	
* [blessings](https://github.com/erikrose/blessings)  
	A library for manipulating terminal windows


## Usage
```
ssants: ~ $ nba_py
Usage: nba_py [OPTIONS] COMMAND [ARGS] ...
  
  A command line interface for pulling NBA stats, scores, and schedules

Options:
  -s 	  Save output to a file
  --help  Show this message and exit.

Commands:
  player  Search for player data
  team    search for team data
```

To get player information and statistics:
`nba_py player <player>`

To get team information and statistics:
`nba_py team <team>`

To save the output to a file
`nba_py -s ...`

## Future work
To get game information
`nba_py game`
 
