# nba_api
There are a couple different sections to this

## Static Methods
`/nba_api/stats/static/`  
This is static stuff embedded in the package and updated as necessary. Functions in here don't send requests over http

### players.py
This module includes functions to access player data
```python
from nba_api.stats.static import players
```
Each of the functions here will return a dictiionary for each player:

| Index        | Value     | Type |
| :----------- | :-------- | :--- |
| 'id'         | player_id | int  |
| 'full_name'  | full_name | str  |
| 'first_name' | first_name| str  |
| 'last_name'  | last_name | str  |

```
get_players()
```
Returns all players

```
find_players_by_full_name(regex_pattern)
```
Returns players whose full name matches the specified pattern

```
find_players_by_first_name(regex_pattern)
````
Returns players whose first name matches the specified pattern

```
find_players_by_last_name(regex_pattern)
```
Returns players whose last name matches the specified pattern

```
find_player_by_id(player_id)
```
Returns a single player whose id matches the specified player id. This function should never return `None`.

##### Example
To look up `LeBron James`, use one of the `find_players_by_` methods
```python
>>> from nba_api.stats.static import players
>>> info = players.find_player_by_full_name("LeBron James")[0]
>>> info
{'full_name': 'LeBron James', 'id': 2554, 'last_name': 'James', 'first_name': 'LeBron'}
```

### teams.py
This module includes functinos to access team data
```python
from nba_api.stats.static import teams
```

Each of the functions here will return a dictiionary for each team:

| Index          | Value        | Type |
| :------------- | :----------- | :--- |
| 'id'           | team_id      | int  |
| 'full_name'    | full_name    | str  |
| 'abbreviation' | abbreviation | str  |
| 'nickname'     | nickname     | str  |
| 'city'         | city         | str  |
| 'state'        | state        | str  |
| 'year_founded' | year_founded | int  |


```
get_teams()
```
Returns all teams

```
find_teams_by_full_name(regex_pattern)
```
Returns teams whose full team name matches the specified regex pattern

```
find_teams_by_state(regex_pattern)
```
Returns teams whose state matches the specified regex pattern

```
find_teams_by_city(regex_pattern)
```
Returns teams whose city matches the specified regex pattern

```
find_teams_by_nickname(regex_pattern)
```
Returns teams whose nickname matches the specified regex pattern

```
find_teams_by_year_founded(year)
```
Returns teams whose year founded matches the specified year

```
find_team_name_by_id(team_id)
```
Returns a single team whose id matches the specified team id. This function should never return `None`.

##### Example
To look up the `Lakers` team, use one of the `find_teams_by_` methods 
```python
>>> from nba.stats.static import teams
>>> info = teams.find_teams_by_city("Los Angeles")
>>> info
[{'full_name': 'Los Angeles Clippers', ... }, {'full_name': 'Los Angeles Lakers', ...}]
```

## Endpoints
`/nba_api/stats/endpoints`  
Every module here contains a class of type `Endpoint` with different parameters and responses.  
Every `Endpoint` will have one or more `Data Sets` as part of its response. 

### Methods
```python
from nba_api.stats.endpoints import *
```
For `Endpoint`s, the response is available as a `json`, `dict` or in raw form:
* `get_json()` or `get_normalized_json()`
* `get_dict()` or `get_normalized_dict()`
* `get_response()`

The available `Data Set`s for an `Endpoint` can be found using 
* `get_available_data()`

For `Data Set`s, the data is available as a `json` or `dict`:
* `get_json()` 
* `get_dict()`

### Endpoints
This is just a partial list of useful endpoints, see the (nba_api docs)[https://github.com/swar/nba_api/tree/master/docs/nba_api/stats/endpoints] for more. 

##### CommonPlayerInfo
Parameters: `player_id`  
Response: `available_seasons`, `common_player_info`, `player_headline_stats`  

##### TeamInfoCommon
Parameters: `team_id`  
Response: `available_seasons`, `team_info_common`, `team_season_ranks`  

### Data Sets
`available_seasons`
```
SEASON_ID
```

`common_player_info`
```
PERSON_ID, FIRST_NAME, LAST_NAME, DISPLAY_FIRST_LAST, DISPLAY_LAST_COMMA_FIRST, DISPLAY_FI_LAST, BIRTHDATE, SCHOOL, COUNTRY, LAST_AFFILIATION, HEIGHT, WEIGHT, SEASON_EXP, JERSEY, POSITION, ROSTERSTATUS, TEAM_ID, TEAM_NAME, TEAM_ABBREVIATION, TEAM_CODE, TEAM_CITY, PLAYERCODE, FROM_YEAR, TO_YEAR, DLEAGUE_FLAG, GAMES_PLAYED_FLAG, DRAFT_YEAR, DRAFT_ROUND, DRAFT_NUMBER
```

`player_headline_stats`
```
PLAYER_ID, PLAYER_NAME, TimeFrame, PTS, AST, REB, PIE 
```

`team_info_common`
```
TEAM_ID, SEASON_YEAR, TEAM_CITY, TEAM_NAME, TEAM_ABBREVIATION, TEAM_CONFERENCE, TEAM_DIVISION, TEAM_CODE, W, L, PCT, CONF_RANK, DIV_RANK, MIN_YEAR, MAX_YEAR
```

`team_season_ranks`
```
LEAGUE_ID, SEASON_ID, TEAM_ID, PTS_RANK, PTS_PG, REB_RANK, REB_PG, AST_RANK, AST_PG, OPP_PTS_RANK, OPP_PTS_PG
```

