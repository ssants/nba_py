import click
import tableprint
from blessings import Terminal 
from nba_api.stats.static.players import find_players_by_full_name 
from nba_api.stats.static.teams import find_teams_by_full_name
from nba_api.stats.endpoints import commonplayerinfo
from nba_api.stats.endpoints import teaminfocommon
from tkinter.filedialog import asksaveasfilename

# Extra data to remove later on
player_trash = ['PERSON_ID', 'FIRST_NAME', 'LAST_NAME', 'DISPLAY_LAST_COMMA_FIRST', 'DISPLAY_FI_LAST','SCHOOL','COUNTRY','LAST_AFFILIATION','SEASON_EXP','TEAM_ID','TEAM_ABBREVIATION','TEAM_CODE','PLAYERCODE','DLEAGUE_FLAG','NBA_FLAG','GAMES_PLAYED_FLAG']
player_rn = { 'DISPLAY_FIRST_LAST':"Name", 'BIRTHDATE':'Birthday', 'HEIGHT':'Height', 'WEIGHT':'Weight', 'JERSEY':'Jersey', 'POSITION':'Position', 'ROSTERSTATUS':'Roster Status', 'TEAM_NAME':'Team', 'TEAM_CITY':'City', 'FROM_YEAR':'From', 'TO_YEAR':'To','DRAFT_YEAR':'Draft Year', 'DRAFT_ROUND':'Draft Round', 'DRAFT_NUMBER':'Draft No.'}

team_trash = ['TEAM_ID','TEAM_CODE']
team_rn = {'SEASON_YEAR':'Season','TEAM_CITY':'City', 'TEAM_NAME':'Name', 'TEAM_ABBREVIATION':'Abbreviation', 'TEAM_CONFERENCE':'Conference','TEAM_DIVISION':'Division','W':'Win', 'L':'Lose', 'CONF_RANK':'Conf Rank', 'DIV_RANK':'Div Rank', 'MIN_YEAR':'From', 'MAX_YEAR':'To'}
headers = ["Key", "Value"]

# blessing terminal interface
term = Terminal()

# If the user would like to save information
savefile = None 
save = False

# Player, Team and Game id for the session
pid = 0
tid = 0
gid = 0
# To indicate another action
nextaction = None

# This command is run first, then it goes to the relevant method below 
@click.group()
@click.option('-s', is_flag=True, help='Save output to a file')
def cli(s):
    """A command line interface for pulling NBA stats, scores, and schedules"""

    tableprint.banner(term.red_on_blue("    NBA PY    ")) 

    # If the user want to save info, set savefile and save variables
    if s:
        global save, savefile
        savefilename = asksaveasfilename()
        if type(savefilename) is  str:
            save = True
            savefile = open(savefilename, 'w') 
            click.echo("Saving output to " + term.green(savefilename))
        else:
            click.echo(term.red("No file specified, not saving"))

# The command for fetching the correct player 
# e.g. `nba_py player lebron`
@cli.command()
@click.argument('p')
def player(p):
    '''Search for player data'''
    global pid, nextaction

    # Search for player, returns a list of zero or more matching
    results = find_players_by_full_name(p)

    # If list is empty, return
    if len(results) == 0: click.echo("No players found"); return

    # If list returns more than one matching player,
    # let the user decide
    if len(results) > 1: 
        with term.location():
            click.echo("More than one player found, please select one:")
            for i in range(0, len(results)):
                click.echo("{0} {1}".format(i+1, results[i]['full_name']))
            p_index = click.prompt("Which player number?", type=int)
            p_index = p_index - 1

        click.echo(term.clear_eos) 

    # If list returns one player, use that 
    else:
        p_index = 0
    pid = results[p_index]['id']

    # Get the player's data
    _get_player_data()

    # If there is another action, do it
    if nextaction == None: return
    if nextaction == "team": _get_team_data()
    if nextaction == "game": pass

# Private method to fetch player data
def _get_player_data():
    global pid, tid, nextaction

    # Sanity check
    if pid == 0: click.echo("Error"); click.pause(); return

    # Get general player info
    playerinfo = commonplayerinfo.CommonPlayerInfo(pid).common_player_info.get_data_frame().iloc[0]

    # Get player name (e.g. "S. Curry") 
    playername = playerinfo['DISPLAY_FI_LAST']

    # Player's team id
    tid = playerinfo['TEAM_ID']

    # Format birthdate 
    bday = playerinfo['BIRTHDATE'].split("-")
    playerinfo['BIRTHDATE'] = "{}/{}/{}".format(bday[1], bday[2][0:2], bday[0]) 

    # Clear out useless data and rename rest
    playerinfo = _fix_labels(playerinfo, player_trash, player_rn)

    # Format data into array for printing
    data = []
    # Also keep track of longest attr for formatting
    maxwidth = 15 
    for k,v in playerinfo.iteritems():
        if len(str(v)) > maxwidth: maxwidth = len(str(v))
        data.append([k,v])
    maxwidth = max(maxwidth, len(playername))

    # Header for table
    # TODO: implement team colors, make nicer
    playerheader = ['Info', playername]

    # Print banner and table
    tableprint.banner(playername, width=(maxwidth*2)+1)
    click.echo(term.move_up*2)
    tableprint.table(data, width=maxwidth, style="fancy_grid")

    # If save
    if save:
        tableprint.banner(playername, width=(maxwidth*2)+1, out=savefile)
        print(term.move_up*2, file=savefile)
        tableprint.table(data, width=maxwidth, style="fancy_grid", out=savefile)

    # Prompt for next action
    nextaction = None
    with term.location():
        click.echo(term.move_y(term.height))
        nowwhat = click.prompt("t for team info | g for games | h for headline stats | q to quit", type=str)
    click.echo(term.move_up*2 + term.clear_eos)

    # Determine next action
    if nowwhat.isalpha():
        if nowwhat[0] == "t":
            nextaction = "team"

        elif nowwhat[0] == "g":
            nextaction = "game"

        elif nowwhat[0] == "h":
            _echo_headline(commonplayerinfo.CommonPlayerInfo(pid).player_headline_stats.get_data_frame().iloc[0])

            with term.location(y=term.height):
                click.pause()
            click.echo(term.move_up + term.clear_eos)

# Private method for printing headline stats
def _echo_headline(h):
    global save

    # Clear out useless data and rename rest
    h = _fix_labels(h, ['PLAYER_ID'], {'PLAYER_NAME':'Name', 'TimeFrame':'Year'})
    # Sometimes player have this attribute
    h = h.rename({'ALL_STAR_APPEARANCES':'All Star App'}) if 'ALL_STAR_APPEARANCES' in h else h

    # Format data into array for printing
    data = []
    # Also keep track of longest attr for formatting
    maxwidth = 10 
    for k,v in h.iteritems():
        maxwidth = max(maxwidth, len(str(k)), len(str(v)))
        data.append([k,v])
    tableprint.table(data, width=maxwidth, style="fancy_grid")

    # If save
    if save: 
        tableprint.table(data, width=maxwidth, style="fancy_grid", out=savefile)

# The command for fetching the correct team
# e.g. `nba_py team lakers`
@cli.command()
@click.argument('t')
def team(t):
    '''Search for team data'''
    global tid; global nextaction

    # Search for team, returns a list of zero or more matching
    results = find_teams_by_full_name(t) 

    # If list is empty, return
    if len(results) == 0: click.echo("No teams found"); return

    # If list returns more than one matching team,
    # let the user decide
    if len(results) > 1: 
        with term.location():
            click.echo("More than one team found, please select one:")
            for i in range(0, len(results)):
                click.echo("{0} {1}".format(i+1, results[i]['full_name']))
            t_index = click.prompt("Which team number?", type=int)
            t_index = t_index - 1
        click.echo(term.clear_eos)

    # If list returns one team, use that
    else:
        t_index = 0
    tid = results[t_index]['id']

    # Ge the team's data
    _get_team_data()

    # If there is another action, do it
    if nextaction == None: return
    if nextaction == "game": pass

# Private method to fetch team data
def _get_team_data():
    global pid, tid, nextaction 

    # Sanity check
    if tid == 0: click.echo("Error"); click.pause(); return

    # Get general team info
    teaminfo = teaminfocommon.TeamInfoCommon(tid).team_info_common.get_data_frame().iloc[0]

    # Get team name
    teamname = teaminfo['TEAM_CITY'] + " " + teaminfo['TEAM_NAME']

    # Clear out useless data and rename rest
    teaminfo = _fix_labels(teaminfo, team_trash, team_rn)

    # Format data into array for printing
    data = []
    # Also keep track of longest attr for formatting
    maxwidth = 15 
    for k,v in teaminfo.iteritems():
        maxwidth = max(maxwidth, len(str(k)), len(str(v)))
        data.append([k,v])
    teamheader = ['Info', teaminfo['City'] + ' ' + teaminfo['Name']]

    # Print table
    tableprint.banner(teamname, width=(maxwidth*2)+1)
    click.echo(term.move_up*2)
    tableprint.table(data, width=maxwidth, style="fancy_grid")

    # If save
    if save: 
        tableprint.banner(teamname, width=(maxwidth*2)+1, out=savefile)
        print(term.move_up*2, file=savefile)
        tableprint.table(data, width=maxwidth, style="fancy_grid", out=savefile)


    # Prompt for next action
    nextaction = None
    with term.location():
        click.echo(term.move_y(term.height))
        nowwhat = click.prompt("g for games | s for season stats | q to quit", type=str)
    click.echo(term.move_up*2 + term.clear_eos)

    # Determine next action
    if nowwhat.isalpha():
        if nowwhat[0] == "g":
            nextaction = "game"
        elif nowwhat[0] == "s":
            _echo_seasonranks(teaminfocommon.TeamInfoCommon(tid).team_season_ranks.get_data_frame().iloc[0],
                    teamheader[1])
            with term.location(y=term.height):
                click.pause()
            click.echo(term.move_up + term.clear_eos)

def _echo_seasonranks(s, teamname):
    # Clear useless data
    s = s.drop(labels=['LEAGUE_ID','SEASON_ID','TEAM_ID'])
    # TODO: rename labels for legibility
    # TODO: learn basketball terms lol

    # Format data into array for printing
    data = []
    # Also keep track of longest attr for formatting
    maxwidth = max(15, len(teamname)) 
    for k,v in s.iteritems():
        maxwidth = max(maxwidth, len(str(k)), len(str(v)))
        data.append([k,v])

    # Print table
    tableprint.banner(teamname, width=(maxwidth*2)+1)
    click.echo(term.move_up*2)
    tableprint.table(data, width=maxwidth, style="fancy_grid")

    # If save
    if save:
        tableprint.table(data, width=maxwidth, style="fancy_grid", out=savefile)

def _fix_labels(info, trashl, renamel):
    return info.drop(labels=trashl).rename(renamel)


